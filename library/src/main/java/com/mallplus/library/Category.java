package com.mallplus.library;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Category extends RealmObject {

    @PrimaryKey
    private int cid;
    private int id;
    private String name;
    private boolean selected = true;

    public Category(){

    }

    public Category(int cid, String name, boolean selected) {
        super();
        this.setCid(cid);
        this.setName(name);
        this.setSelected(selected);
    }

    public Category(int cid, int id, String name) {
        super();
        this.setCid(cid);
        this.setId(id);
        this.setName(name);
    }


    public int getCid() {
        return cid;
    }

    public void setCid(int id) {
        this.cid = id;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id;  }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}