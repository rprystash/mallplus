package com.mallplus.library;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MarkJRust on 10/16/16.
 */

public class Store implements Parcelable{
    private String name;
    private String description;
    private String operatingHours;
    private String storeDetailPhotoLink;
    private String storeImageIconLink;
    private Integer storeID;
    private Integer catID;
    private double distance;

    // Parcel -> Store
    public static Creator<Store> CREATOR = new Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel source) {
            return new Store(source);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };

    public Store(String name, String description, String operatingHours, String storeDetailPhotoLink, String storeImageIconLink, Integer storeID, Integer catID) {
        this.name = name;
        this.description = description;
        this.operatingHours = operatingHours;
        this.setStoreDetailPhotoLink(storeDetailPhotoLink);
        this.setStoreImageIconLink(storeImageIconLink);
        this.setStoreID(storeID);
        this.setCatID(catID);
    }

    public Store(Parcel parcel) {
        //NOTE: Order is the same as `writeToParcel`
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.operatingHours = parcel.readString();
        this.storeDetailPhotoLink=parcel.readString();
        this.storeID = parcel.readInt();
        this.catID = parcel.readInt();
        this.distance = parcel.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // Store -> Parcel
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Write each value to the parcel
        // ORDER MATTERS! MATCH UP TOP!
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(operatingHours);
        dest.writeString(storeDetailPhotoLink);
        dest.writeInt(storeID);
        dest.writeInt(catID);
        dest.writeDouble(distance);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) { this.distance = distance; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperatingHours() {
        return operatingHours;
    }

    public void setOperatingHours(String operatingHours) {
        this.operatingHours = operatingHours;
    }

    public String getStoreDetailPhotoLink() {
        return storeDetailPhotoLink;
    }

    public void setStoreDetailPhotoLink(String storeDetailPhotoLink) {
        this.storeDetailPhotoLink = storeDetailPhotoLink;
    }

    public String getStoreImageIconLink() {
        return storeImageIconLink;
    }

    public void setStoreImageIconLink(String storeImageIconLink) {
        this.storeImageIconLink = storeImageIconLink;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

    public Integer getCatID() {
        return catID;
    }

    public void setCatID(Integer catID) {
        this.catID = catID;
    }
}
