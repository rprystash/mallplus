package com.mallplus.library;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MarkJRust on 11/22/16.
 */

public class Deal implements Parcelable {

    private String dealDescription;
    private String dealPrice;
    private String dealTitle;
    private String dealImg;

    public Deal (String dealTitle, String dealDescription, String dealPrice, String dealImg){
        this.setDealTitle(dealTitle);
        this.setDealDescription(dealDescription);
        this.setDealPrice(dealPrice);
        this.setDealImg(dealImg);
    }

    // Parcel -> Deal
    public static Creator<Deal> CREATOR = new Creator<Deal>() {
        @Override
        public Deal createFromParcel(Parcel source) {
            return new Deal(source);
        }

        @Override
        public Deal[] newArray(int size) {
            return new Deal[size];
        }
    };

    public Deal(Parcel parcel) {
        //NOTE: Order is the same as `writeToParcel`
        this.setDealTitle(parcel.readString());
        this.setDealDescription(parcel.readString());
        this.setDealPrice(parcel.readString());
    }

    // Store -> Parcel
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Write each value to the parcel
        // ORDER MATTERS! MATCH UP TOP!
        dest.writeString(getDealTitle());
        dest.writeString(getDealDescription());
        dest.writeString(getDealPrice());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getDealDescription() {
        return dealDescription;
    }

    public void setDealDescription(String dealDescription) {
        this.dealDescription = dealDescription;
    }

    public String getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(String dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealTitle() {
        return dealTitle;
    }

    public void setDealTitle(String dealTitle) {
        this.dealTitle = dealTitle;
    }

    public String getDealImg() {
        return dealImg;
    }

    public void setDealImg(String dealImg) {
        this.dealImg = dealImg;
    }
}
