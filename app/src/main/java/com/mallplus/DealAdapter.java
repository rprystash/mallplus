package com.mallplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mallplus.library.Deal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by MarkJRust on 11/25/16.
 */

public class DealAdapter extends BaseAdapter {
    private Context context;
    private List<Deal> deals;


    public DealAdapter(Context context) {
        this.context = context;
        this.deals = new ArrayList<>();
    }

    public void add(Deal deal) {
        deals.add(deal);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        deals.remove(position);
        notifyDataSetChanged();
    }

    public void swapDeals(List<Deal> deals) {
        this.deals = deals;
        notifyDataSetChanged();
    }

    public void clearDeals()
    {
        deals.clear();
    }

    public void switchDeals(Deal deal1, Deal deal2) {
        Collections.swap(
                deals,
                deals.indexOf(deal1),
                deals.indexOf(deal2)
        );
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return deals.size();
    }

    @Override
    public Object getItem(int position) {
        return deals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        DealAdapter.DealViewHolder viewHolder;


        // Convert view is null, this is the first row
        if(convertView == null) {
            //
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_deal, viewGroup, false);
            viewHolder = new DealAdapter.DealViewHolder(convertView);
            // Set view holder as tag
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (DealAdapter.DealViewHolder) convertView.getTag();
        }

        Deal currentDeal = deals.get(position);
        viewHolder.bindDeal(currentDeal);

        return convertView;
    }

    public class DealViewHolder {
        //private TextView dealTitle;
        //private TextView dealDescription;
        private ImageView dealImg;


        public DealViewHolder(View view) {
            //dealTitle = (TextView) view.findViewById(R.id.deal_title);
            //dealDescription = (TextView) view.findViewById(R.id.deal_description);
            dealImg = (ImageView)view.findViewById(R.id.deal_img);
        }

        public void bindDeal(Deal deal) {
            //dealTitle.setText(deal.getDealTitle());
            //dealDescription.setText(deal.getDealDescription());
            Context context = dealImg.getContext();
            Glide.with(context).load(deal.getDealImg()).into(dealImg);

        }
    }
}
