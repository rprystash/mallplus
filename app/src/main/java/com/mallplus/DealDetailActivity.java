package com.mallplus;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import net.glxn.qrgen.android.QRCode;
import com.mallplus.library.Deal;
import android.graphics.Bitmap;
import android.widget.ImageView;


public class DealDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_detail);

        Deal deal = getIntent().getParcelableExtra(StoreDetailActivity.ARG_DEAL);

        TextView lblName = (TextView)findViewById(R.id.deal_detail_name);
        TextView lblDescription = (TextView)findViewById(R.id.deal_detail_description);
        TextView lblPrice = (TextView)findViewById(R.id.deal_detail_price);


        lblName.setText(deal.getDealTitle());
        lblDescription.setText(deal.getDealDescription());
        lblPrice.setText("$"+deal.getDealPrice());


//        ImageView qrCode = (ImageView) findViewById(R.id.deal_qrCode);
//       Glide.with(this).load("http://www.qrstuff.com/images/sample.png").into(qrCode);
        
/*        Bitmap myBitmap = QRCode.from("http://www.qrstuff.com/images/sample.png").bitmap();
        ImageView myImage = (ImageView) findViewById(R.id.deal_qrCode);
        myImage.setImageBitmap(myBitmap);*/

        Bitmap myBitmap = QRCode.from(deal.getDealTitle()).withSize(250, 250).bitmap();
        ImageView myImage = (ImageView) findViewById(R.id.deal_qrCode);
        myImage.setImageBitmap(myBitmap);


    }

}
