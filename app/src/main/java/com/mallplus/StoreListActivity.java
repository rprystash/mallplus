package com.mallplus;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mallplus.library.Store;

public class StoreListActivity extends StoreHelper {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_list);
        toolBar();

        final StoreAdapter storeAdapter = new StoreAdapter(this);
        fillListView(storeAdapter);
        fab_Click(storeAdapter);
    }

    private void fillListView(final StoreAdapter storeAdapter){
        getBeacons(storeAdapter);

        // Get Listview
        final ListView listView = (ListView) findViewById(R.id.store_list_view);

        // Set adapter
        listView.setAdapter(storeAdapter);

        // Set click
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // Get store
                Store store = (Store) storeAdapter.getItem(position);

                // Create intent for store
                Intent intent = new Intent(StoreListActivity.this, StoreDetailActivity.class);
                intent.putExtra(StoreDetailActivity.ARG_STORE, store);
                startActivity(intent);
            }
        });
    }
}
