package com.mallplus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import com.mallplus.library.Category;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmResults;

public class CategorySelectActivity extends AppCompatActivity {

    MyCustomAdapter dataAdapter = null;

    ArrayList<Category> CategoryList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_select);

        //Generate list View from ArrayList
        displayListView();

        checkButtonClick();

    }

    static boolean getVisible(int cid, Context context){
        Realm myRealm = Realm.getInstance(context);
        Category cat = myRealm.where(Category.class).equalTo("cid", cid).findFirst();

        if (cat != null)
            return cat.isSelected();

        return true;
    }

    private void displayListView() {
        Context context = getApplicationContext();

        try {
            CategoryList = StoreHelper.getCats();

            for (Category cat : CategoryList) {
                cat.setSelected(getVisible(cat.getCid(), context));
            }
        }catch(JSONException jex){

        }


        //create an ArrayAdaptar from the String Array
        dataAdapter = new MyCustomAdapter(this,
                R.layout.content_category_select, CategoryList);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);




        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox1);
                cb.toggle();
                Category Category = (Category) cb.getTag();
                Category.setSelected(cb.isChecked());
            }
        });

    }

    private class MyCustomAdapter extends ArrayAdapter<Category> {

        private ArrayList<Category> CategoryList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Category> CategoryList) {
            super(context, textViewResourceId, CategoryList);
            this.CategoryList = new ArrayList<Category>();
            this.CategoryList.addAll(CategoryList);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.content_category_select, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;
                        Category Category = (Category) cb.getTag();
                        Category.setSelected(cb.isChecked());
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Category Category = CategoryList.get(position);
            holder.code.setText(" (" +  Category.getCid() + ")");
            holder.name.setText(Category.getName());
            holder.name.setChecked(Category.isSelected());
            holder.name.setTag(Category);

            return convertView;

        }

    }

    private void checkButtonClick() {

        Button myButton = (Button) findViewById(R.id.findSelected);
        myButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                StringBuffer responseText = new StringBuffer();

                ArrayList<Category> CategoryList = dataAdapter.CategoryList;

                Realm myRealm = Realm.getInstance(getApplicationContext());
                RealmResults<Category> results = myRealm.where(Category.class).findAll();
                myRealm.beginTransaction();
                results.clear();
                myRealm.commitTransaction();

                for(int i=0;i<CategoryList.size();i++){
                    Category category = CategoryList.get(i);
                    myRealm.beginTransaction();
                        Category cat = myRealm.copyToRealm(category);
                    myRealm.commitTransaction();
                }

                Toast.makeText(getApplicationContext(), "Scanning for your interests...", Toast.LENGTH_LONG).show();

                finish();
            }

        });

    }
}
