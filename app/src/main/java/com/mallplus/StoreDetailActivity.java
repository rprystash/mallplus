package com.mallplus;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mallplus.library.Deal;
import com.mallplus.library.Store;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class StoreDetailActivity extends AppCompatActivity {

    public static final String ARG_STORE = "store";
    public static final String ARG_DEAL = "deal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_detail);

        // Get store
        Store store = getIntent().getParcelableExtra(ARG_STORE);

        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Display store name
        if(store != null) {
            TextView lblTitle = (TextView)findViewById(R.id.store_name);
            TextView lblDescription = (TextView)findViewById(R.id.store_description);
            TextView lblOperatingHrs = (TextView)findViewById(R.id.store_hours);

            //TODO get Dom's details in with my list
            lblTitle.setText(store.getName());
            lblDescription.setText(store.getDescription());
            lblOperatingHrs.setText(store.getOperatingHours());

            //TODO use store.getStoreID() and query deals
            String dealsJSON= getStoreJSONByStoreID(store.getStoreID());
            List<Deal> deals;
            try {
                //List Stuff
                // Create our adapter
                final DealAdapter dealAdapter = new DealAdapter(this);

                deals = parseDealJSON(dealsJSON);
                dealAdapter.swapDeals(deals);

                // Get Listview
                final ListView listView = (ListView) findViewById(R.id.deal_list_view);
                // Set adapter
                listView.setAdapter(dealAdapter);

                // Set click
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        // Get deal
                        Deal deal = (Deal) dealAdapter.getItem(position);

                        // Create intent for deal
                        Intent intent = new Intent(StoreDetailActivity.this, DealDetailActivity.class);
                        intent.putExtra(StoreDetailActivity.ARG_DEAL, deal);
                        startActivity(intent);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            //TODO get Photo details in with my list
            //Set ImageView
            ImageView banner = (ImageView) findViewById(R.id.store_image);
            String storeDetail = store.getStoreDetailPhotoLink();
            //TODO figure out hwo to get a fukin default image set
            if(storeDetail == null) {
                Glide.with(this).load("https://i.ytimg.com/vi/yaqe1qesQ8c/maxresdefault.jpg").into(banner);
            } else {
                Glide.with(this).load(storeDetail).into(banner);
            }
        }
    }

    private String getStoreJSONByStoreID(Integer storeID) {
        return StoreHelper.getJSON("http://dev-mallplus-cms.pantheonsite.io/node.json?type=deal&field_store="+storeID.toString());
    }

    private String getFileJSON(Integer fileID) {
        return StoreHelper.getJSON("http://dev-mallplus-cms.pantheonsite.io/file/"+fileID.toString()+".json");
    }

    private String getFileById(int fileID) throws  JSONException{
        return (new JSONObject(getFileJSON(fileID))).getString("url");
    }

    //Parse JSON to return a List of deal objects
    private List<Deal> parseDealJSON(String jsonString) throws JSONException {
        JSONObject obj = new JSONObject(jsonString);
        JSONArray storeInformationArray = obj.getJSONArray("list");

        List<Deal> deals = new ArrayList<>();
        for(int i=0; i<storeInformationArray.length(); i++)
        {
            JSONObject dealObject = storeInformationArray.getJSONObject(i);
            String dealTitle = dealObject.getString("title");
            String dealDescription = dealObject.getString("field_description");
            String dealPrice = dealObject.getString("field_price");

            String dealImg = new String();
            try {
                String json = dealObject.getString("field_image");
                JSONObject o = new JSONObject(json);
                JSONObject file = new JSONObject(o.getString("file"));
                int imgId = file.getInt("id");
                dealImg = getFileById(imgId);
            }
            catch(Exception ex){

            }

            deals.add(new Deal(dealTitle,dealDescription,dealPrice,dealImg));
        }
        return deals;

    }

}
