package com.mallplus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.Utils;
import com.mallplus.library.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class StoreHelper extends AppCompatActivity {

    private BeaconManager beaconManager;
    private Region region;
    ArrayList<Category> CategoryList;

    protected void toolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    protected void fab_Click(final StoreAdapter sa){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Enter store location(s).");

                // Set up the input
                final EditText input = new EditText(view.getContext());

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String input_text = input.getText().toString();
                        String[] beacons = input_text.split(",");

                        List<Store> stores = new ArrayList<Store>();

                        /// 3.01, 3.02, 3.03
                        for (String beacon : beacons) {
                            stores.add(getStoreObject(beacon));
                        }

                        for (Store store : stores)
                        {
                            if (store.getStoreID() == -1 || !CategorySelectActivity.getVisible(store.getCatID(), getApplicationContext()))
                            {
                                stores.remove(store);
                            }
                        }
                        sa.swapStores(stores);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    protected void getBeacons(final StoreAdapter storeAdapter) {
        //Beacon Stuff
        beaconManager = new BeaconManager(this);
        beaconManager.setForegroundScanPeriod(500, 0);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                Runnable r = new Thread_GetBeacons(list, storeAdapter);
                new Thread(r).start();
            }
        });
        region = new Region("ranged region", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);
    }

    void fillCats() throws JSONException{
        ArrayList<Category> CategoryList = getCats();

        this.CategoryList = getCats();
    }

    static ArrayList<Category> getCats() throws JSONException{
        ArrayList<Category> CategoryList = new ArrayList<Category>();

        JSONObject obj = new JSONObject(getCatJSON());
        JSONArray CatArray = obj.getJSONArray("list");

        for(int i = 0; i < CatArray.length(); i++){
            JSONObject catObject = CatArray.getJSONObject(i);
            CategoryList.add(new Category(catObject.getInt("field_category_number"), catObject.getInt("nid"), catObject.getString("title")));
        }

        return CategoryList;
    }

    public class Thread_GetBeacons implements Runnable {

        StoreAdapter storeAdapter;
        List<Beacon> list;

        public Thread_GetBeacons(List<Beacon> list, StoreAdapter storeAdapter) {
            this.list = list;
            this.storeAdapter = storeAdapter;
        }

        public void run() {
            final List<Store> stores = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {
                // Add store for this beacon

                Beacon beacon = list.get(i);

                int major = beacon.getMajor();
                int minor = beacon.getMinor();
                double distance = Utils.computeAccuracy(beacon);

                String spaceID = major + "." + String.format(Locale.ENGLISH, "%02d", minor);

                Store store = getStoreObject(spaceID);
                store.setDistance(distance);

                if (CategorySelectActivity.getVisible(store.getCatID(), getApplicationContext())) {
                    stores.add(store);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    storeAdapter.swapStores(stores);
                }
            });
        }
    }

    int getCatId(int Id){
        try {
            if (CategoryList == null)
                fillCats();
        }
        catch (Exception ex){

        }

        if (CategoryList != null) {
            for (Category category : CategoryList) {
                if (category.getId() == Id) {
                    return category.getCid();
                }
            }
        }

        return -1;
    }

    protected Store getStoreObject(String beaconID) {
        HashMap<String, String> returnedHashMap = new HashMap<>();
        try {
            String jsonReturn = getStoreJSON(beaconID);
            returnedHashMap = parseStoreJSON(jsonReturn);
        } catch (Exception ex) {
            //TODO: Catch exception
        }

        Integer storeID = -1;
        if(returnedHashMap.get("storeID") != null) {
            storeID = Integer.parseInt(returnedHashMap.get("storeID"));
        }

        Integer catID = -1;
        if(returnedHashMap.get("catIDID") != null) {
            Integer CatIDID = Integer.parseInt(returnedHashMap.get("catIDID"));
            catID = getCatId(CatIDID);
        }


        Store parsedStore = new Store(returnedHashMap.get("storeName"),
                returnedHashMap.get("storeDescription"),
                returnedHashMap.get("storeHours"),
                returnedHashMap.get("storeDetailPhoto"),
                returnedHashMap.get("storeImageIcon"),
                storeID,
                catID);
        //TODO add storeID to store object

        return parsedStore;
    }

    private String getStoreJSON(String spaceID) {
        return getJSON("https://dev-mallplus-cms.pantheonsite.io/node.json?type=store&field_space=" + spaceID.toString());
    }

    private static String getCatJSON() {
        return getJSON("https://dev-mallplus-cms.pantheonsite.io/node.json?type=category");
    }

    public static String getJSON(String _url) {
        StringBuilder out = new StringBuilder();
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL(_url);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-Type", "application/hal+json");
                urlConnection.setChunkedStreamingMode(0);

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    out.append(line);
                }
            } finally {
                urlConnection.disconnect();
            }
        } catch (IOException ex) {
        }

        return out.toString();
    }

    private HashMap<String, String> parseStoreJSON(String jsonString) throws JSONException {
        HashMap<String, String> myHashMap = new HashMap<>();

        JSONObject obj = new JSONObject(jsonString);
        JSONArray storeInformationArray = obj.getJSONArray("list");
        JSONObject storeInformationObject = storeInformationArray.getJSONObject(0);

        myHashMap.put("storeName", storeInformationObject.getString("title"));
        myHashMap.put("storeHours", storeInformationObject.getString("field_operating_hours"));
        myHashMap.put("storeDescription", storeInformationObject.getString("field_description"));
        myHashMap.put("storeImageIcon", storeInformationObject.getString("field_icon_link"));
        myHashMap.put("storeDetailPhoto", storeInformationObject.getString("field_header_link"));
        myHashMap.put("storeID", (storeInformationObject.getString("nid")));
        myHashMap.put("catIDID", new JSONObject(storeInformationObject.getString("field_category_ddl")).getString("id"));

        return myHashMap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this);
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onPause() {
        beaconManager.stopRanging(region);

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(StoreHelper.this, CategorySelectActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
