package com.mallplus;


import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.guo.duoduo.randomtextview.RandomTextView;
import com.mallplus.library.Store;


public class StoreRadarActivity extends StoreHelper
{


    RandomTextView randomTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radar_main);
        //toolBar();

        randomTextView = (RandomTextView) findViewById(R.id.random_textview);

        final StoreAdapter storeAdapter = new StoreAdapter(this);
        storeAdapter.registerDataSetObserver(new DataSetObserver()
        {
            @Override
            public void onChanged()
            {

                randomTextView.clearStores();

                for(Store store : storeAdapter.getStores()){
                    randomTextView.addStore(store);
                }
                randomTextView.show();
            }
        });

        fillRadar(storeAdapter);
        fab_Click(storeAdapter);
    }


    private void fillRadar(final StoreAdapter storeAdapter){
        getBeacons(storeAdapter);

        randomTextView.setOnRippleViewClickListener(
                new RandomTextView.OnRippleViewClickListener()
                {
                    @Override
                    public void onRippleViewClicked(View view, int position)
                    {
                    //    StoreRadarActivity.this.startActivity(
                    //            new Intent(StoreRadarActivity.this, RefreshProgressActivity.class));

                        // Get store
                        Store store = (Store) storeAdapter.getItem(position);

                        // Create intent for store
                        Intent intent = new Intent(StoreRadarActivity.this, StoreDetailActivity.class);
                        intent.putExtra(StoreDetailActivity.ARG_STORE, store);
                        startActivity(intent);
                    }
                });

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                randomTextView.show();
            }
        }, 2 * 1000);
    }
}
