package com.mallplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mallplus.library.Store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StoreAdapter extends BaseAdapter {
    private Context context;
    private List<Store> stores;

    private long updateDuration = 5000;
    private long lastUpdateTime;

    public StoreAdapter(Context context) {
        this.context = context;
        this.stores = new ArrayList<>();

        // Set last update time to now.
        lastUpdateTime = System.currentTimeMillis();
    }

    public List<Store> getStores(){
        return stores;
    }

    public void add(Store store) {
        stores.add(store);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        stores.remove(position);
        notifyDataSetChanged();
    }

    public void swapStores(List<Store> stores) {
        // If it has been less than 5 seconds, don't update
        if((System.currentTimeMillis() - lastUpdateTime) < updateDuration) {
            return;
        }

        this.stores = stores;
        notifyDataSetChanged();
        lastUpdateTime = System.currentTimeMillis();
    }

    public void clearStores()
    {
        stores.clear();
    }

    public void switchStores(Store store1, Store store2) {
        Collections.swap(
                stores,
                stores.indexOf(store1),
                stores.indexOf(store2)
        );
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return stores.size();
    }

    @Override
    public Object getItem(int position) {
        return stores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        StoreViewHolder viewHolder;

        // Convert view is null, this is the first row
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_store, viewGroup, false);
            viewHolder = new StoreViewHolder(convertView);
            // Set view holder as tag
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (StoreViewHolder) convertView.getTag();
        }

        Store currentStore = stores.get(position);
        viewHolder.bindStore(currentStore);

        return convertView;
    }

    public class StoreViewHolder {
        private TextView storeName;
        private TextView storeHours;
        private ImageView storeIconPhoto;

        public StoreViewHolder(View view) {
            storeName = (TextView) view.findViewById(R.id.store_name);
            storeHours = (TextView) view.findViewById(R.id.store_hours);
            storeIconPhoto = (ImageView) view.findViewById(R.id.storeImageIcon);
        }

        public void bindStore(Store store) {
            storeName.setText(store.getName());
            storeHours.setText(store.getOperatingHours());
            Context context = storeIconPhoto.getContext();
            Glide.with(context).load(store.getStoreImageIconLink()).into(storeIconPhoto);
        }
    }

}
